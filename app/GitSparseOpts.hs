module GitSparseOpts where

import Options.Applicative

data Options = Options { optCommand     :: Maybe OptCommand
                       , optShowVersion :: Bool }
              deriving (Show)

data OptCommand = AddCmd { dirs :: [String] }
                | DisableCmd
                  deriving (Show)

addCmdOption :: Parser OptCommand
addCmdOption = AddCmd <$> many (argument str (metavar "DIRS ..."))

disableCmdOption :: Parser OptCommand
disableCmdOption = pure DisableCmd

gpoption :: Parser Options
gpoption = Options
       <$> optional (subparser ( command "add"     (info addCmdOption
                                                     (progDesc "Add directories to sparse-checkout file"))
                              <> command "disable" (info disableCmdOption
                                                     (progDesc "Disable sparse-checkout"))))
       <*> switch ( short 'v' <> long "version" <> help "Show version and exit" )
