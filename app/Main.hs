{-# LANGUAGE RecordWildCards #-}
module Main where

import           Data.Version        (showVersion)
import           GitCmd
import qualified GitSparseFree       as GSFree
import           GitSparseOpts
import           Options.Applicative
import           Paths_git_sparse

main :: IO ()
main = do
        opts <- execParser wrappedOpt
        run opts
    where
        wrappedOpt = info (helper <*> gpoption)
            ( fullDesc
           <> progDesc "Sparse checkout helper"
           <> header   "Sparse checkout")
        run Options{..} = if optShowVersion
                              then putStrLn $ "git-sparse version: " ++ showVersion version
                              else case optCommand of
                                       Nothing            -> GSFree.editSparseCheckout
                                       (Just (AddCmd ds)) -> GSFree.appendToSparseCheckout ds
                                       (Just DisableCmd)  -> GSFree.disableSparseCheckout
