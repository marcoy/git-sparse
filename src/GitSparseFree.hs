{-# LANGUAGE OverloadedStrings #-}
module GitSparseFree where

import           Control.Monad      (liftM)
import           Control.Monad.Free (Free (..), foldFree, liftF)
import           Data.Monoid        ((<>))
import qualified Data.Text          as T
import           Shelly             (Sh (..), appendfile, cmd, fromText, get_env, liftIO, mkdir_p, rm_f, shelly,
                                     silently)
import qualified Shelly             as Shelly
import           System.Directory   (getCurrentDirectory)
import           System.Exit        (ExitCode (..))
import           System.FilePath    ((</>))
import           System.Process     (rawSystem)

type Editor = T.Text

data GitSparse a = AddEntries [String] FilePath a
                 | Echo T.Text a
                 | Disable FilePath a
                 | GitDir (T.Text -> a)
                 | MkDir FilePath a
                 | RunEditor Editor FilePath (ExitCode -> a)
                 | SelectEditor Editor (T.Text -> a)
                 | SparseToggle Bool a

instance Functor GitSparse where
    fmap f (AddEntries  es fp x) = AddEntries es fp (f x)
    fmap f (Disable      fp   x) = Disable fp (f x)
    fmap f (Echo         m    x) = Echo m (f x)
    fmap f (GitDir            g) = GitDir (f . g)
    fmap f (MkDir        p    x) = MkDir p (f x)
    fmap f (RunEditor    e fp g) = RunEditor e fp (f . g)
    fmap f (SelectEditor e    g) = SelectEditor e (f . g)
    fmap f (SparseToggle b    x) = SparseToggle b (f x)


sparseOn :: Bool
sparseOn = True

sparseOff :: Bool
sparseOff = False

shellyFp :: String -> Shelly.FilePath
shellyFp = fromText . T.pack


gitDir :: Free GitSparse FilePath
gitDir = liftF $ GitDir T.unpack

selectEditor :: Editor -> Free GitSparse Editor
selectEditor defaultEditor = liftF $ SelectEditor defaultEditor id

mkDir :: FilePath -> Free GitSparse ()
mkDir fp = liftF $ MkDir fp ()

vimEditor :: Free GitSparse Editor
vimEditor = selectEditor "vim"

sparseCheckoutToggle :: Bool -> Free GitSparse ()
sparseCheckoutToggle on_off = liftF $ SparseToggle on_off ()

runEditor :: Editor -> FilePath -> Free GitSparse ExitCode
runEditor editor file = liftF $ RunEditor editor file id

addEntries :: [String] -> FilePath -> Free GitSparse ()
addEntries entries sparseCheckoutFile = liftF $ AddEntries entries sparseCheckoutFile ()

echoMsg :: T.Text -> Free GitSparse ()
echoMsg msg = liftF $ Echo msg ()

disableSparse :: FilePath -> Free GitSparse ()
disableSparse fp = liftF $ Disable fp ()


-- | Natural Transformation from @Free GitSparse a@ to @Sh a@.
shellify :: GitSparse a -> Sh a
shellify (AddEntries es fp v) = traverse (appendfile (shellyFp fp) . (<> "\n") . T.pack) es >> return v
shellify (Echo       msg   v) = Shelly.echo msg >> return v
shellify (Disable       fp v) = rm_f (shellyFp fp) >> return v
shellify (GitDir           f) = f . T.strip <$> cmd "git" "rev-parse" "--git-dir"
shellify (MkDir         fp v) = mkdir_p (shellyFp fp) >> return v
shellify (RunEditor   e fp f) = liftIO $ rawSystem (T.unpack e) [fp] >>= return . f
shellify (SelectEditor   e f) = get_env "EDITOR" >>= return . (maybe (f e) f)
shellify (SparseToggle  on v) = cmd "git" "config" "core.sparseCheckout" bValue >> return v
                                  where bValue = if on then "true" else "false"

-- | Natural Transformation from @Free GitSparse a@ to @IO a@ through @Sh a@.
gitSparseIO :: Free GitSparse a -> IO a
gitSparseIO f = shelly . silently $ foldFree shellify f

getSparseCheckoutFileF :: Free GitSparse FilePath
getSparseCheckoutFileF = do
    sparseCheckoutToggle sparseOn
    gitRoot <- gitDir
    let gitInfo      = gitRoot </> "info"
        checkoutFile = gitInfo </> "sparse-checkout"
    mkDir gitInfo
    return checkoutFile

editSparseCheckoutFileF :: Free GitSparse ExitCode
editSparseCheckoutFileF = do
    sparseCheckoutFile <- getSparseCheckoutFileF
    editor <- vimEditor
    runEditor editor sparseCheckoutFile

appendToSparseCheckoutF :: [String] -> Free GitSparse ()
appendToSparseCheckoutF entries = do
    sparseCheckoutFile <- getSparseCheckoutFileF
    addEntries entries sparseCheckoutFile

disableSparseCheckoutF :: Free GitSparse ()
disableSparseCheckoutF = do
    sparseCheckoutFile <- getSparseCheckoutFileF
    sparseCheckoutToggle sparseOff
    disableSparse sparseCheckoutFile

readTreeMsg :: Free GitSparse ()
readTreeMsg = echoMsg "Run `git read-tree -mu HEAD' to update the working tree"


-- | = Main entry points
editSparseCheckout :: IO ()
editSparseCheckout = gitSparseIO $ do
                       exitCode <- editSparseCheckoutFileF
                       case exitCode of
                         ExitSuccess     -> readTreeMsg
                         (ExitFailure _) -> echoMsg "Editor did not exit cleanly"

appendToSparseCheckout :: [String] -> IO ()
appendToSparseCheckout entries = gitSparseIO $ do
                                   appendToSparseCheckoutF entries
                                   readTreeMsg

disableSparseCheckout :: IO ()
disableSparseCheckout = gitSparseIO $ do
                          disableSparseCheckoutF
                          readTreeMsg
