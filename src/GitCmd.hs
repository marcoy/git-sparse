{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE ScopedTypeVariables  #-}
module GitCmd where

import           Control.Monad
import           Data.Monoid    ((<>))
import           Data.Text      as T
import           Shelly
import           System.Exit
import           System.Process
default (T.Text)

class GitReturnType r where
    git' :: [Text] -> r

instance GitReturnType (Sh Text) where
    git' args = run "git" args

instance GitReturnType r => GitReturnType (Text -> r) where
    git' args = \v -> git' (args ++ [v])

git :: (GitReturnType r) => r
git = git' []


gitdir :: Sh Text
gitdir = cmd "git" "rev-parse" "--git-dir"

sparseEnabled :: Sh Bool
sparseEnabled = do
        result <- catchany_sh (run "git" ["config", "core.sparseCheckout"])
                              (\_ -> return "false")
        if strip result == "true"
            then return True
            else return False

enableSparseCheckout :: Sh ()
enableSparseCheckout = cmd "git" "config" "core.sparseCheckout" "true"

disableSparseCheckout :: Sh ()
disableSparseCheckout = cmd "git" "config" "core.sparseCheckout" "false"

echoReadTreeInfo :: Sh ()
echoReadTreeInfo = do
        echo "Run `git read-tree -mu HEAD' to update the working tree"
        echo "Any local changes will be gone. Stash first!!!"


setupSparseCheckoutFile :: Sh Text
setupSparseCheckoutFile = do
        whenM (not <$> sparseEnabled) enableSparseCheckout
        gdir <- T.strip <$> gitdir
        gitInfoDir <- toTextWarn $ gdir </> "info"
        sparseCheckoutFile <- toTextWarn $ gitInfoDir </> "sparse-checkout"
        mkdir_p $ fromText gitInfoDir
        return sparseCheckoutFile

editSparseCheckout :: IO ()
editSparseCheckout = shelly $ silently $ do
        sparseCheckoutFile <- setupSparseCheckoutFile
        editor <- chooseEditor
        exitCode <- liftIO $ rawSystem (T.unpack editor) (T.unpack <$> [sparseCheckoutFile])
        case exitCode of
            ExitSuccess     -> echoReadTreeInfo
            (ExitFailure _) -> echo "Editor did not exit cleanly"
    where
        chooseEditor :: Sh Text
        chooseEditor = do
            edEnv <- get_env "EDITOR"
            case edEnv of
                (Just e) -> return e
                Nothing  -> return "vim"

appendToSparseCheckout :: [String] -> IO ()
appendToSparseCheckout []   = shelly $ silently $ do
        echo "Nothing to add... Exiting"

appendToSparseCheckout dirs = shelly $ silently $ do
        sparseCheckoutFile <- setupSparseCheckoutFile
        echo $ "Modifying " <> sparseCheckoutFile <> ":"
        forM_ dirs $ \d -> do
            echo $ "Adding " <> (T.pack d)
            appendfile (fromText sparseCheckoutFile) $ (T.pack d) <> "\n"
        echo ""
        echoReadTreeInfo

clearSparseCheckout :: IO ()
clearSparseCheckout = shelly $ silently $ do
        sparseCheckoutFile <- setupSparseCheckoutFile
        echo $ "Clearing " <> sparseCheckoutFile
        writefile (fromText sparseCheckoutFile) ""
        echo ""
        echoReadTreeInfo

disableSparseCheckoutIO :: IO ()
disableSparseCheckoutIO = shelly $ silently $ do
        disableSparseCheckout
        echo $ "sparse checkout disabled"
        echoReadTreeInfo
